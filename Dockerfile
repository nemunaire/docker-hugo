FROM alpine:edge

RUN apk add --no-cache \
        git \
        go \
	hugo
